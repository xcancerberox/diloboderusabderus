from diloboderusabderus.gaits.wave import GaitWave


class GaitTripod(GaitWave):
    def __init__(self, initial_configuration, final_configuration, stride):
        super().__init__(initial_configuration, final_configuration, stride, beta=6)

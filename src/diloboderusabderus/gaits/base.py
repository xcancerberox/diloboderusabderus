from diloboderusabderus.robot_configuration import RobotConfiguration
from diloboderusabderus.mylogging import logger


class Gait():
    def __init__(self, height, distance, stride, beta):
        self.distance = distance
        self.stride = stride
        self.height = height
        self.beta = beta
        self.full_cycle = 12

        self.support_speed = stride/beta
        self.transfer_speed = stride/(self.full_cycle - beta)

        self.support_initial_position = []
        self.transfer_initial_position = []
        self.legs_phase = [None]*6
        self.legs_support_range = [None]*6
        self.body_position_displacement = None
        self.body_rotation_displacement = None

        self.kinematic_model = None

    def get_remaining_steps_on(self, step):
        support_pattern = self.support_pattern_for(step)
        remaining_steps = []

        for leg in range(6):
            if leg in support_pattern:
                if leg == 3:
                    steps = self.beta
                else:
                    steps = (self.beta - (self.full_cycle - self.legs_phase[leg])) % self.full_cycle
            else:
                if leg == 3:
                    steps = self.full_cycle - self.beta
                else:
                    steps = ((self.full_cycle - self.beta) - (self.full_cycle -
                             (self.beta + self.legs_phase[leg]))) % self.full_cycle
            remaining_steps.append(steps)
        logger.info('Remaining steps for step {}: {}'.format(step, remaining_steps))
        return remaining_steps

    def support_pattern_for(self, step):
        support_pattern = []
        for leg in range(6):
            if self.legs_support_range[leg] is None:
                raise ValueError

            if step in self.legs_support_range[leg]:
                support_pattern.append(leg)

        logger.info("Support pattern for step {}: {}".format(step, support_pattern))
        return support_pattern

    def get_config(self):
        if self.kinematic_model is None:
            raise ValueError

        if self.body_position_displacement is None:
            raise ValueError

        if self.body_rotation_displacement is None:
            raise ValueError

        new_config = self.base_config.copy()
        step = 0
        step_displacement = self.displacement/(self.full_cycle/2)
        full_step_posible = True
        while full_step_posible:
            for step in range(self.full_cycle):
                # Simetric support_pattern. Generate new body position every half cycle
                if step == 0 or step == (self.full_cycle/2):
                    logger.info('Gait generator new body pose for step {}'.format(step))
                    self.kinematic_model.position = new_config.body_position.copy()
                    self.kinematic_model.rotation = new_config.body_rotation.copy()
                    new_config.body_position = self.kinematic_model.relative_to_absolute_position(
                                                                        self.body_position_displacement)
                    new_config.body_rotation = new_config.body_rotation.copy() + self.body_rotation_displacement

                new_config.support_pattern = self.support_pattern_for(step)

                for leg in range(6):
                    if leg in new_config.support_pattern:
                        new_config.legs_point_position[leg] = self.transfer_initial_position[leg]
                    else:
                        new_config.legs_point_position[leg] = self.support_initial_position[leg]

                yield new_config
                self.distance -= self.displacement/(self.full_cycle/2)
                logger.info('Remaining distance {}'.format(self.distance))

                if self.distance < step_displacement:
                    full_step_posible = False
                    break

    def calculate_support_range(self):
        for leg in range(6):
            support_init = self.legs_phase[leg]
            support_end = (self.legs_phase[leg] + self.beta) % self.full_cycle
            if support_init > support_end:
                self.legs_support_range[leg] = [i for i in range(support_init, self.full_cycle)]
                self.legs_support_range[leg].extend([i for i in range(0, support_end)])
            else:
                self.legs_support_range[leg] = [i for i in range(support_init, support_end)]

import numpy as np

from diloboderusabderus.mylogging import logger


def circular_trajectory_gen(radius, center, steps=100):
    step = np.pi*2/steps
    for phy in np.arange(0, np.pi*2, step):
        yield np.array([center[0] + radius*np.cos(phy),
                        center[1] + radius*np.sin(phy),
                        center[2],
                        1])


def linear_trajectory_gen(initial_position, final_position, steps=100):
    logger.info('Linear trajectory (steps {}) ({}) -> ({})'.format(steps, initial_position, final_position))
    direction = final_position - initial_position
    distance = np.linalg.norm(direction)
    if abs(distance) == 0:
        for i in range(steps):
            yield initial_position
    else:
        direction = direction/distance
        step = distance/steps
        for x in np.arange(step, distance + step, step):
            yield initial_position + direction * x


def triangular_trajectory_gen(initial_position, final_position, steps=100):
    direction = final_position - initial_position
    distance = direction[1]
    middle_position = initial_position.copy()
    middle_position[1] += distance/2
    middle_position[2] += abs(distance/2)

    half_steps = steps//2
    for position in linear_trajectory_gen(initial_position, middle_position, half_steps):
        yield position

    for position in linear_trajectory_gen(middle_position, final_position, steps - half_steps):
        yield position


def body_linear_trajectory(initial_config, final_config, steps=100):
    logger.info('Body linear trajectory (steps {}) ({}) -> ({})'.format(steps, initial_config, final_config))

    steps = int(steps)
    new_config = initial_config.copy()
    position_diff = final_config.body_position - initial_config.body_position
    position_distance = np.linalg.norm(position_diff)
    if position_distance == 0:
        logger.info('Body linear trajectory without translation')
        position_direction = position_diff
        x = np.zeros(steps)
    else:
        logger.info('Body linear trajectory translation distance {}'.format(position_distance))
        position_direction = position_diff/position_distance
        step = position_distance/steps
        x = np.arange(step, position_distance + step, step)

    rotation_diff = (final_config.body_rotation - initial_config.body_rotation)[0]
    rotation_distance = abs(rotation_diff)
    if rotation_distance == 0:
        logger.info('Body linear trajectory without rotation')
        rotation_direction = rotation_diff
        f = np.zeros(steps)
    else:
        logger.info('Body linear trajectory rotation distance {}'.format(rotation_distance))
        rotation_direction = rotation_diff/rotation_distance
        step = rotation_distance/steps
        f = np.arange(step, rotation_distance + step, step)

    for i in range(steps):
        logger.info('Body linear trajectory step {}'.format(i))
        new_config.body_position = initial_config.body_position + position_direction * x[i]
        new_config.body_rotation = initial_config.body_rotation + np.array((rotation_direction * f[i], 0, 0, 0))
        yield new_config


def body_circular_trajectory(radius, center_config, steps=100):
    config = center_config.copy()
    step = np.pi*2/steps

    for phy in np.arange(0, np.pi*2, step):
        config.body_position = np.array([center_config.body_position[0] + radius*np.cos(phy),
                                         center_config.body_position[1] + radius*np.sin(phy),
                                         center_config.body_position[2],
                                         1])
        yield config

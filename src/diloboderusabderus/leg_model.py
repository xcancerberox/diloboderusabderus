"""
3DOF Leg Kinematic model using kctools engine models

"""
from kctools.kcengine import transformations
from kctools.kcengine.models import kcvol3dof

from diloboderusabderus.cartesian_coordinates import CartesianCoordinates


class Leg(CartesianCoordinates):
    def __init__(self, position, rotation, lengths):
        super().__init__(position, rotation)
        self.engine = kcvol3dof.KCVol3DOF(*lengths)

    def forward_kinematics(self, angles):
        if not len(angles) == 3:
            raise ValueError
        relative_position = self.engine.forward_kinematics(*angles)
        return self.relative_to_absolute_position(relative_position)

    def inverse_kinematics(self, point_position):
        return self.engine.inverse_kinematics(point_position)

    def reachable(self, point_position):
        return self.engine.reachable(point_position)

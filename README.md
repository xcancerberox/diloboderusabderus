# DiloboderusAbderus

Distributed system for control, visualization and management of a hexapod robot


# How to run visualizer

```
$ virtualenv -p python3 venv
$ source venv/bin/activate
(venv)$ pip install .
(venv)$ python bin/visualizer.py
```

For a testing show, in a different console
```
$ source venv/bin/activate
(venv)$ python bin/inplace_show.py
```

import unittest
import numpy as np

import kctools.inverse_kinematics_solution as iks
from kctools.testing import CustomAssertions

from diloboderusabderus.leg_model import Leg


class TestLeg(unittest.TestCase, CustomAssertions):
    def setUp(self):
        self.leg = Leg(np.array((0, 0, 0, 1)), np.array((0, 0, 0, 1)), lengths=[10, 10, 10])

    def test_leg_forward_kinematics_on_rotated_coordinates(self):
        cases = [
                {
                    'position': np.array((0, 0, 0, 1)),
                    'rotation': np.array((np.radians(90), 0, 0, 1)),
                    'angles': (0, 0, 0),
                    'expected': (0, 30, 0, 1)
                    },
                {
                    'position': np.array((0, 0, 0, 1)),
                    'rotation': np.array((-np.radians(90), 0, 0, 1)),
                    'angles': (0, 0, 0),
                    'expected': (0, -30, 0, 1)
                    },
                {
                    'position': np.array((0, 0, 0, 1)),
                    'rotation': np.array((np.radians(90), 0, 1, 0)),
                    'angles': (0, 0, 0),
                    'expected': (0, 0, -30, 1)
                    },
                {
                    'position': np.array((0, 0, 0, 1)),
                    'rotation': np.array((-np.radians(90), 0, 1, 0)),
                    'angles': (0, 0, 0),
                    'expected': (0, 0, 30, 1)
                    },
                {
                    'position': np.array((0, 0, 0, 1)),
                    'rotation': np.array((np.radians(90), 1, 0, 0)),
                    'angles': (0, 0, 0),
                    'expected': (30, 0, 0, 1)
                    },
                {
                    'position': np.array((0, 0, 0, 1)),
                    'rotation': np.array((-np.radians(90), 1, 0, 0)),
                    'angles': (0, 0, 0),
                    'expected': (30, 0, 0, 1)
                    },
                ]

        for index, case in enumerate(cases):
            with self.subTest(i=index):
                self.leg.position = np.array(case['position'])
                self.leg.rotation = case['rotation']
                calculated = self.leg.forward_kinematics(case['angles'])
                np.testing.assert_almost_equal(case['expected'], calculated)

    def test_leg_inverse_kinematics(self):
        cases = [
                {
                    'position': np.array((0, 0, 0, 1)),
                    'rotation': np.array((0, 0, 0, 1)),
                    'point_position': np.array((30, 0, 0, 1)),
                    'expected': iks.IKSolution([0, iks.IKSolution([0, 0], [0, 0])], [None])
                    },
                {
                    'position': np.array((0, 0, 0, 1)),
                    'rotation': np.array((0, 0, 0, 1)),
                    'point_position': np.array((0, 30, 0, 1)),
                    'expected': iks.IKSolution([np.radians(90), iks.IKSolution([0, 0], [0, 0])], [None])
                    },
                ]

        for index, case in enumerate(cases):
            with self.subTest(i=index):
                self.leg.position = case['position']
                self.leg.rotation = case['rotation']
                calculated = self.leg.inverse_kinematics(case['point_position'])
                self.assertSolutionAlmostEqual(calculated, case['expected'])

    def test_leg_inverse_kinematics_base_translated(self):
        length = 10
        absolute_test_point = np.array((length*np.sqrt(2)+length, 0, 0, 1))

        leg = Leg(np.array((0, 0, 0, 1)), np.array((0, 0, 0, 1)), lengths=[length, length, length])
        relative_test_point = leg.absolute_to_relative_position(absolute_test_point)
        calculated = leg.inverse_kinematics(relative_test_point)
        expected = iks.IKSolution([0, iks.IKSolution([-np.pi/4, np.pi/2], [np.pi/4, -np.pi/2])], [None])
        self.assertSolutionAlmostEqual(calculated, expected)

        leg_tranlated = Leg(np.array((0, 0, length*np.sqrt(2), 1)), np.array((0, 0, 0, 1)),
                            lengths=[length, length, length])
        relative_test_point = leg_tranlated.absolute_to_relative_position(absolute_test_point)
        calculated = leg_tranlated.inverse_kinematics(relative_test_point)
        expected = iks.IKSolution([0, iks.IKSolution([np.pi/4, 0], [np.pi/4, 0])], [None])
        self.assertSolutionAlmostEqual(calculated, expected)

    def test_leg_inverse_kinematics_base_rotated(self):
        length = 10
        absolute_test_point = np.array((length*np.sqrt(2)+length, 0, 0, 1))

        leg = Leg(np.array((0, 0, 0, 1)), np.array((0, 0, 0, 1)), lengths=[length, length, length])
        relative_test_point = leg.absolute_to_relative_position(absolute_test_point)
        calculated = leg.inverse_kinematics(relative_test_point)
        expected = iks.IKSolution([0, iks.IKSolution([-np.pi/4, np.pi/2], [np.pi/4, -np.pi/2])], [None])
        self.assertSolutionAlmostEqual(calculated, expected)

        leg_rotated = Leg(np.array((0, 0, 0, 1)), np.array((np.radians(45), 0, 0, 1)),
                          lengths=[length, length, length])
        relative_test_point = leg_rotated.absolute_to_relative_position(absolute_test_point)
        calculated = leg_rotated.inverse_kinematics(relative_test_point)
        expected = iks.IKSolution([-np.radians(45), iks.IKSolution([-np.pi/4, np.pi/2], [np.pi/4, -np.pi/2])], [None])
        self.assertSolutionAlmostEqual(calculated, expected)


class TestLegReachable(unittest.TestCase, CustomAssertions):
    def setUp(self):
        self.leg = Leg(np.array((0, 0, 0, 1)), np.array((0, 0, 0, 1)), lengths=[10, 10, 10])

    def test_leg_reachable_endpoint(self):
        self.assertTrue(self.leg.reachable(np.array((30, 0, 0, 1))))


if __name__ == '__main__':
    unittest.main()

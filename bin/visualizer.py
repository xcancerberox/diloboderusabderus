import numpy as np
import sys
import imp

from diloboderusabderus.diloboderus import DiloboderusAbderusVisualizer
from diloboderusabderus.robot_configuration import RobotConfiguration


if __name__ == '__main__':
    morphology_config = imp.load_source('', sys.argv[1])

    body_position = np.array((0, 0, (morphology_config.legs_lengths[1] +
                                     morphology_config.legs_lengths[2])/1.3, 1))
    body_rotation = np.array((0, 0, 0, 1))
    support_pattern = [0, 1, 2, 3, 4, 5]
    legs_point_position = [np.array((morphology_config.legs_lengths[0]*1.5,
                                     0,
                                     -(morphology_config.legs_lengths[1] +
                                       morphology_config.legs_lengths[2])/1.3,
                                     1))]*6
    initial_robot_config = RobotConfiguration(body_position, body_rotation, support_pattern, legs_point_position)

    dilo = DiloboderusAbderusVisualizer(initial_robot_config, morphology_config)
    dilo.run()

import os
import socket
import serial
import struct
import time
import numpy as np


transStart = 128
transEnd = 129
requestEcho = 0  # [order]
echo = 1  # [order]

# Function
# Request supply voltage
requestSupplyVoltage = 10  # [order]
# Respond supply voltage
supplyVoltage = 11  # [order] [voltage * 100 / 128] [voltage * 100 % 128]
# Request change I/O port state
requestChangeIO = 20  # [order] [IOindex] [1/0]

# Installation
requestMoveLeg = 30  # [order] [leg] [64 + dx] [64 + dy] [64 + dz]
requestCalibrate = 32  # [order]

# Blocking orders, range is 64 ~ 127

# Installation
requestInstallState = 64  # [order]
requestCalibrateState = 66  # [order]
requestBootState = 68  # [order]
requestCalibrateVerify = 70  # [order]

# Simple action
requestCrawlForward = 80  # [order]
requestCrawlBackward = 82  # [order]
requestCrawlLeft = 84  # [order]
requestCrawlRight = 86  # [order]
requestTurnLeft = 88  # [order]
requestTurnRight = 90  # [order]
requestActiveMode = 92  # [order]
requestSleepMode = 94  # [order]
requestSwitchMode = 96  # [order]

# Complex action
requestCrawl = 110  # [order] [64 + x] [64 + y] [64 + angle]
requestChangeBodyHeight = 112  # [order] [64 + height]
requestMoveBody = 114  # [order] [64 + x] [64 + y] [64 + z]
requestRotateBody = 116  # [order] [64 + x] [64 + y] [64 + z]
requestTwistBody = 118  # [order] [64 + xMove] [64 + yMove] [64 + zMove] [64 + xRotate] [64 + yRotate] [64 + zRotate]
requestSetAngles = 119  # [order]
requestSetFullAngles = 120  # [order]

# Universal responded orders, range is 21 ~ 127
# These orders are used to respond orders without proprietary response orders.

orderStart = 21  # [order]
orderDone = 23  # [order]


class FreenoveSerialCommunication():
    def __init__(self, serial_device):
        self.serial = serial.Serial(serial_device, 115200)
        time.sleep(2)
        self.serial.flush()

    def write(self, data):
        self.serial.write(data)

    def read(self):
        data = []
        state = 'HEADER'

        while True:
            readed = ord(self.serial.read(1))
            if state == 'HEADER':
                if readed == transStart:
                    data.append(readed)
                    state = 'PAYLOAD'
                else:
                    pass
            elif state == 'PAYLOAD':
                data.append(readed)
                if readed == transEnd:
                    print('Packet complete')
                    break
            else:
                raise ValueError
        return data

    def close(self):
        self.serila.close()


class FreenoveTCPCommunication():
    BUFFER_SIZE = 1024

    def __init__(self, ip, port):
        self.channel = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.channel.connect((ip, port))

    def write(self, data):
        self.channel.send(data)

    def read(self):
        data = self.channel.recv(self.BUFFER_SIZE)
        return data

    def close(self):
        self.channel.close()


class FreenoveHexapodKit():
    def __init__(self):
        self.interface = None
        for dev_name in ['/dev/ttyACM0', '/dev/ttyACM1']:
            if os.path.exists(dev_name):
                print('Connectin serial interface {}'.format(dev_name))
                self.interface = FreenoveSerialCommunication(dev_name)
                break

        ip = '192.168.4.1'
        port = 65535
        if self.interface is None:
            print('Connectin TCP ip: {}, port: {}'.format(ip, port))
            self.interface = FreenoveTCPCommunication(ip, port)

    def check_angles_limits(self, angles):
        angles_ok = True
        for index, leg_angles in enumerate(angles):
            leg = index + 1
            if leg in [2, 5]:
                if (leg_angles[0] > 120) or (leg_angles[0] < 60):
                    angles_ok = False
                    break
            else:
                if (leg_angles[0] > 135) or (leg_angles[0] < 45):
                    angles_ok = False
                    break
            if (leg_angles[1] > 140) or (leg_angles[1] < 45):
                angles_ok = False
                break
            if (leg_angles[2] > 140) or (leg_angles[2] < 45):
                angles_ok = False
                break

        if not angles_ok:
            raise ValueError("Legs angles out of boundaries {}: {}".format(leg, leg_angles))

    def to_degrees(self, angles):
        angles_deg = []
        for index, leg_angles in enumerate(angles):
            leg = index + 1
            if leg in [1, 2, 3]:
                angles_deg.append([int(90 - np.degrees(leg_angles[0])),
                                   int(90 + np.degrees(leg_angles[1])),
                                   int(180 - np.degrees(leg_angles[2]))])
            else:
                angles_deg.append([int(90 - np.degrees(leg_angles[0])),
                                   int(90 - np.degrees(leg_angles[1])),
                                   int(np.degrees(leg_angles[2]))])
        return angles_deg

    def send_full_angles(self, angles):
        angles_deg = self.to_degrees(angles)
        self.check_angles_limits(angles_deg)
        packet = struct.pack(
                        'BB'+'B'*3*6+'B',
                        transStart,
                        requestSetFullAngles,
                        angles_deg[0][0],
                        angles_deg[0][1],
                        angles_deg[0][2],
                        angles_deg[1][0],
                        angles_deg[1][1],
                        angles_deg[1][2],
                        angles_deg[2][0],
                        angles_deg[2][1],
                        angles_deg[2][2],
                        angles_deg[3][0],
                        angles_deg[3][1],
                        angles_deg[3][2],
                        angles_deg[4][0],
                        angles_deg[4][1],
                        angles_deg[4][2],
                        angles_deg[5][0],
                        angles_deg[5][1],
                        angles_deg[5][2],
                        transEnd
                        )
        self.interface.write(packet)

    def send_angles_by_leg(self, angles):
        angles_deg = self.to_degrees(angles)
        self.check_angles_limits(angles_deg)
        for index, leg_angles in enumerate(angles_deg):
            leg = index + 1
            packet = struct.pack(
                            'BBBBBBB',
                            transStart,
                            requestSetAngles,
                            leg,
                            leg_angles[0],
                            leg_angles[1],
                            leg_angles[2],
                            transEnd
                            )
            self.interface.write(packet)

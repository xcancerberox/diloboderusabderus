import pickle
import sys
import zmq

from freenove import FreenoveHexapodKit

if __name__ == '__main__':
    ctx = zmq.Context()
    configuration_sub = ctx.socket(zmq.SUB)
    configuration_sub.connect("tcp://127.0.0.1:5556")
    configuration_sub.setsockopt(zmq.SUBSCRIBE, b'angles')

    robot = FreenoveHexapodKit()

    while True:
        try:
            key, data = configuration_sub.recv_multipart()
            angles = pickle.loads(data)
            print('New angles: ', angles)
            robot.send_full_angles(angles)
        except KeyboardInterrupt:
            break

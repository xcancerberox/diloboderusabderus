import zmq
import time
import pickle
import numpy as np
from cmd import Cmd

import diloboderusabderus.body_trajectory_generator as tgen

context = zmq.Context()
socket_pub = context.socket(zmq.PUB)
socket_pub.bind('tcp://127.0.0.1:5556')
position = 0


def send_linear_trajectory(initial_position, final_position, velocity):
    for position in tgen.linear_trajectory_gen(initial_position, final_position):
        socket_pub.send_multipart([b'position', pickle.dumps(position)])
        time.sleep(1/velocity)


def send_circular_trajectory(radius, center_position, velocity):
    for position in tgen.circular_trajectory_gen(radius, center_position):
        socket_pub.send_multipart([b'position', pickle.dumps(position)])
        time.sleep(1/velocity)


def send_rotation(initial_angle, final_angle, direction, ccw, velocity, step=0.1):
    for rotation in (np.arange(initial_angle, final_angle, step)*ccw):
        socket_pub.send_multipart([b'rotation', pickle.dumps([np.radians(rotation)] + direction)])
        time.sleep(1/velocity)


def send_rotation_x(initial_angle, final_angle, velocity, step=0.1):
    send_rotation(initial_angle, final_angle, [1, 0, 0], velocity, step)


def send_rotation_y(initial_angle, final_angle, velocity, step=0.1):
    send_rotation(initial_angle, final_angle, [0, 1, 0], velocity, step)


def send_rotation_z(initial_angle, final_angle, velocity, step=0.1):
    send_rotation(initial_angle, final_angle, [0, 0, 1], velocity, step)


while True:
    initial_position = np.array((0, 0, 60, 1))
    socket_pub.send_multipart([b'position', pickle.dumps([0, 0, 60, 1])])
    socket_pub.send_multipart([b'rotation', pickle.dumps([0, 0, 0, 1])])
    socket_pub.send_multipart([b'rotation', pickle.dumps([0, 0, 1, 0])])
    socket_pub.send_multipart([b'rotation', pickle.dumps([0, 1, 0, 0])])

    time.sleep(1)
    send_linear_trajectory(initial_position, np.array((0, 10, 60, 1)), 100)
    send_linear_trajectory(np.array((0, 10, 60, 1)), np.array((0, -10, 60, 1)), 100)
    send_linear_trajectory(np.array((0, -10, 60, 1)), np.array((0, 0, 60, 1)), 100)
    send_linear_trajectory(np.array((0, 0, 60, 1)), np.array((8, 0, 60, 1)), 100)

    send_circular_trajectory(8, initial_position, 100)

    send_linear_trajectory(np.array((8, 0, 60, 1)), initial_position, 100)

    send_rotation_z(0, 10, 100, 1)
    send_rotation_z(-10, 10, 100, -1)
    send_rotation_z(-10, 0, 100, 1)

    send_rotation_y(0, 10, 100, 1)
    send_rotation_y(-10, 10, 100, -1)
    send_rotation_y(-10, 0, 100, 1)

    send_rotation_x(0, 10, 100, 1)
    send_rotation_x(-10, 10, 100, -1)
    send_rotation_x(-10, 0, 100, 1)

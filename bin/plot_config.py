import imp
import pickle
import sys
import zmq
import numpy as np
import matplotlib
matplotlib.use('Gtk3Agg')
import matplotlib.pyplot as plt

from diloboderusabderus.hexapod_model import Hexapod


def aquisition():
    ctx = zmq.Context()
    configuration_sub = ctx.socket(zmq.SUB)
    configuration_sub.connect("tcp://127.0.0.1:5556")
    configuration_sub.setsockopt(zmq.SUBSCRIBE, b'configuration')

    configs = []

    print('Start data aquisition')
    while True:
        try:
            key, data = configuration_sub.recv_multipart()
            config = pickle.loads(data)
            print('new config: ', config)
            configs.append(config)
        except KeyboardInterrupt:
            break
    return configs


def add_support_pattern_markers(ax, support_pattern_change):
    for t_change, pattern in support_pattern_change:
        # Menos uno por que esta config ya esta con el support pattern nuevo, queda mejor la linea en el anterior
        ax.axvline((t_change - 1)/morphology_config.velocity, linestyle='-.', color='#000000', linewidth=0.5)


def add_support_pattern_labels(ax, support_pattern_change):
    for t_change, pattern in support_pattern_change:
        # Menos uno por que esta config ya esta con el support pattern nuevo, queda mejor la linea en el anterior
        ylim = ax.get_ylim()
        ax.text((t_change + 1)/morphology_config.velocity, abs(ylim[1] + ylim[0])/2, str(pattern), rotation='vertical')


def add_leg_on_support_span(leg, ax, support_pattern_change, alpha=0.2):
    for index, (t_change, pattern) in enumerate(support_pattern_change):
        if leg in pattern:
            # Menos uno por que esta config ya esta con el support pattern nuevo, queda mejor la linea en el anterior
            if len(support_pattern_change) > index + 1:
                ax.axvspan((t_change - 1)/morphology_config.velocity,
                           (support_pattern_change[index + 1][0] - 1)/morphology_config.velocity,
                           color='#000000', alpha=alpha)
            else:
                ax.axvspan((t_change - 1)/morphology_config.velocity,
                           ax.get_xlim()[1],
                           color='#000000', alpha=alpha)


def plot(configs):
    t = [i/morphology_config.velocity for i in range(len(configs))]

    body = [[], [], []]
    body_angle = []
    stability = []
    legs = [
            [[], [], []],
            [[], [], []],
            [[], [], []],
            [[], [], []],
            [[], [], []],
            [[], [], []],
            ]
    angles = [
            [[], [], []],
            [[], [], []],
            [[], [], []],
            [[], [], []],
            [[], [], []],
            [[], [], []],
            ]
    last_support_pattern = configs[0].support_pattern
    support_pattern_change = [(0, configs[0].support_pattern)]
    for index, config in enumerate(configs):
        hexapod = Hexapod(config.body_position, config.body_rotation, morphology_config)

        if not (last_support_pattern == config.support_pattern):
            support_pattern_change.append((index, config.support_pattern))
            last_support_pattern = config.support_pattern

        body[0].append(config.body_position[0])
        body[1].append(config.body_position[1])
        body[2].append(config.body_position[2])
        body_angle.append(np.degrees(config.body_rotation[0]))
        stability.append(hexapod.stability(config))
        for i in range(6):
            legs[i][0].append(config.legs_point_position[i][0])
            legs[i][1].append(config.legs_point_position[i][1])
            legs[i][2].append(config.legs_point_position[i][2])
        res = hexapod.inverse_kinematics(config)
        for i in range(6):
            angles[i][0].append(np.degrees(res[i][0]))
            angles[i][1].append(np.degrees(res[i][1]))
            angles[i][2].append(np.degrees(res[i][2]))

    plt.style.use('seaborn-darkgrid')

    fig, (ax1, ax2, ax3) = plt.subplots(3, 1, constrained_layout=True)
    plt.suptitle("Posición y rotación del cuerpo relativa al mundo", size='xx-large')
    ax1.plot(t, body[0], 'b', label='x')
    ax1.plot(t, body[1], 'r', label='y')
    ax1.plot(t, body[2], 'g', label='z')
    legend = ax1.legend(loc='best', fancybox=True, frameon=1)
    frame = legend.get_frame()
    frame.set_facecolor('#ffffff')
    add_support_pattern_markers(ax1, support_pattern_change)
    ax1.set_title('Posición del cuerpo')
    ax1.set_xlabel('Tiempo [s]')
    ax1.set_ylabel('Posición [cm]')

    ax2.plot(t, body_angle, 'b')
    add_support_pattern_markers(ax2, support_pattern_change)
    ax2.set_title('Rotación del cuerpo sobre el eje {}'.format(config.body_rotation[1:]))
    ax2.set_xlabel('Tiempo [s]')
    ax2.set_ylabel('Rotación [deg]')

    ax3.plot(t, stability, 'b')
    add_support_pattern_markers(ax3, support_pattern_change)
    add_support_pattern_labels(ax3, support_pattern_change)
    ax3.set_title('Margen de estabilidad')
    ax3.set_xlabel('Tiempo [s]')
    ax3.set_ylabel('Margen de estabilidad [cm]')

    fig, axes = plt.subplots(2, 3, constrained_layout=True)
    plt.suptitle("Posición relativa de las patas", size='xx-large')
    for i, row in enumerate(axes):
        for j, ax in enumerate(row):
            ax.plot(t, legs[i+j][0], 'b', label='x')
            ax.plot(t, legs[i+j][1], 'r', label='y')
            ax.plot(t, legs[i+j][2], 'g', label='z')
            if i + j == 0:
                legend = ax.legend(loc='best', fancybox=True, frameon=1)
                frame = legend.get_frame()
                frame.set_facecolor('#ffffff')
            add_support_pattern_markers(ax, support_pattern_change)
            add_leg_on_support_span((i*3+j), ax, support_pattern_change)
            ax.set_title('Pata {}'.format(i*3+j))
            ax.set_xlabel('Tiempo [s]')
            ax.set_ylabel('Posición [cm]')

    fig, axes = plt.subplots(2, 3, constrained_layout=True)
    plt.suptitle("Ángulo de los actuadores de cada pata", size='xx-large')
    for i, row in enumerate(axes):
        for j, ax in enumerate(row):
            ax.plot(t, angles[i+j][0], 'b', label='theta_1')
            ax.plot(t, angles[i+j][1], 'r', label='theta_2')
            ax.plot(t, angles[i+j][2], 'g', label='theta_3')
            if i + j == 0:
                legend = ax.legend(loc='best', fancybox=True, frameon=1)
                frame = legend.get_frame()
                frame.set_facecolor('#ffffff')
            add_support_pattern_markers(ax, support_pattern_change)
            add_leg_on_support_span((i*3+j), ax, support_pattern_change)
            ax.set_title('Pata {}'.format(i*3+j))
            ax.set_xlabel('Tiempo [s]')
            ax.set_ylabel('Angulo [rad]')
    plt.show()


if __name__ == '__main__':
    morphology_config = imp.load_source('', sys.argv[1])

    configs = aquisition()
    plot(configs)
